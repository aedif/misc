import tkinter as tk
import os
from functools import partial
import win32gui
from config_manager import Config

config = Config()

default_main_path = config.find("default_paths/irfan_image_sort")
list_of_irfan_paths = []

m = tk.Tk()
frm_sorting_buttons = tk.Frame()

# Path related widgets
lbl_main_path = tk.Label(text="Path to main:")
ent_main_path = tk.Entry()
ent_main_path.insert(0, default_main_path)


def retrieveAllIrfanPaths(hwnd, lParam):
    title = win32gui.GetWindowText(hwnd)
    if title.find(" - IrfanView") > 0:
        global list_of_irfan_paths
        list_of_irfan_paths.append(title[0 : title.find(" - IrfanView")])


def moveImageToFolder(folder):
    global list_of_irfan_paths
    list_of_irfan_paths = []
    win32gui.EnumWindows(retrieveAllIrfanPaths, 0)

    for irfan_path in list_of_irfan_paths:
        move_to_path = (
            f"{ent_main_path.get()}\\{folder}\\{os.path.basename(irfan_path)}"
        )

        # rename if file already exists in destination
        i = 0
        while os.path.exists(move_to_path):
            name = os.path.splitext(os.path.basename(move_to_path))
            if len(name) is 1:
                move_to_path = f"{ent_main_path.get()}\\{folder}\\{name[0]}_{i}"
            else:
                move_to_path = (
                    f"{ent_main_path.get()}\\{folder}\\{name[0]}_{i}{name[1]}"
                )
            i += 1

        if os.path.exists(irfan_path) and not os.path.exists(move_to_path):
            os.rename(irfan_path, move_to_path)
            print(f"Moved {irfan_path} to {move_to_path}")


def removeImage():
    global list_of_irfan_paths
    list_of_irfan_paths = []
    win32gui.EnumWindows(retrieveAllIrfanPaths, 0)
    for irfan_path in list_of_irfan_paths:
        if os.path.exists(irfan_path):
            os.remove(irfan_path)
            print(f"REMOVED {irfan_path}")


def generateButtons():
    # remove old buttons
    global frm_sorting_buttons
    frm_sorting_buttons.destroy()
    frm_sorting_buttons = tk.Frame()

    path = ent_main_path.get()
    dirs = [dI for dI in os.listdir(path) if os.path.isdir(os.path.join(path, dI))]

    for folder in dirs:
        btn_mv = tk.Button(
            text=folder,
            width=25,
            command=partial(moveImageToFolder, folder),
            master=frm_sorting_buttons,
        )
        btn_mv.pack()

    btn_rmv = tk.Button(
        text="REMOVE",
        width=25,
        bg="red",
        command=partial(removeImage),
        master=frm_sorting_buttons,
    )
    btn_rmv.pack()

    frm_sorting_buttons.pack()


btn_main_path = tk.Button(text="Generate Buttons", command=generateButtons)

lbl_main_path.pack()
ent_main_path.pack()
btn_main_path.pack()

m.mainloop()

