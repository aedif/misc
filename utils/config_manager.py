import json


class Config:
    def __init__(self):
        self.config_json = {}
        with open("..\\config.json") as f:
            self.config_json = json.load(f)

    def find(self, path: str):
        """ Retrieves element using path notation 
            e.g. "default_paths/irfan_image_sort """

        keys = path.split("/")
        rv = self.config_json
        for key in keys:
            rv = rv[key]
        return rv
